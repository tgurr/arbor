(
    app-admin/eclectic[~scm]
    app-admin/s6-exherbo[~scm]
    app-arch/libarchive[~scm]
    app-misc/screen[~scm]
    app-editors/e4r[~scm]
    app-shells/bash-completion[~scm]
    app-shells/zsh[~scm]
    app-text/djvu[~scm]
    app-vim/exheres-syntax[~scm]
    dev-db/xapian-core[~scm]
    dev-lang/clang[~7-scm]
    dev-lang/clang[~scm]
    dev-lang/llvm[~7-scm]
    dev-lang/llvm[~scm]
    dev-libs/compiler-rt[~scm]
    dev-libs/oblibs[~scm]
    sys-libs/openmp[~scm]
    dev-libs/fmt[~scm]
    dev-libs/pinktrace[~scm]
    dev-libs/pugixml[~scm]
    dev-ruby/ruby-filemagic[~scm]
    dev-scm/git-remote-helpers[~scm]
    dev-util/exherbo-dev-tools[~scm]
    dev-util/ltrace[~scm]
    dev-util/systemtap[~scm]
    dev-util/tig[~scm]
    dev-util/valgrind[~scm]
    media-libs/jbig2dec[~scm]
    net-irc/irssi[~scm]
    net-wireless/iw[~scm]
    net-wireless/wireless-regdb[~scm]
    net-wireless/wpa_supplicant[~scm]
    net-www/elinks[~scm]
    sys-apps/dbus[~scm]
    sys-apps/eudev[~scm]
#    sys-apps/paludis[~scm]
    sys-apps/multiload[~scm]
    sys-apps/sydbox[~scm]
    sys-apps/systemd[~scm]
    sys-boot/dracut[~scm]
    sys-boot/efibootmgr[~scm]
    sys-boot/grub[~scm]
    sys-devel/meson[~scm]
    sys-devel/ninja[~scm]
    sys-fs/btrfs-progs[~scm]
    sys-libs/libc++[~scm]
    sys-libs/libc++abi[~scm]
    sys-libs/musl[~scm]
    sys-libs/musl-compat[~scm]
) [[
    *author = [ Exherbo developers ]
    *token = scm
    *description = [ Mask scm versions ]
]]

dev-util/ccache [[
    author = [ Thomas Anderson <tanderson@caltech.edu> ]
    date = [ 14 Apr 2015 ]
    token = broken
    description = [
        UNSUPPORTED: Results in subtle, often undetectable breakage. Don't use it to compile packages or you get to keep both pieces.
    ]
]]

(
    dev-libs/openssl[<1.0.2q]
    dev-libs/openssl[>=1.1&<1.1.1a]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 27 Nov 2018 ]
    *token = security
    *description = [ CVE-2018-0734, CVE-2018-0735, CVE-2018-5407 ]
]]

media-libs/jasper[<1.900.20] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2016-10251 ]
]]

dev-db/mysql[<5.7.22] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Sep 2018 ]
    token = security
    description = [ Oracle Critical Patch Update Advisory - July 2018 ]
]]

net-misc/curl[<7.62.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Nov 2018 ]
    token = security
    description = [ CVE-2018-16839, CVE-2018-16840, CVE-2018-16842 ]
]]

app-admin/sudo[<1.8.20_p1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 30 Mar 2017 ]
    token = security
    description = [ CVE-2017-1000367 ]
]]

(
    media-libs/libpng:1.2[<1.2.57]
    media-libs/libpng:1.5[<1.5.28]
    media-libs/libpng:1.6[<1.6.27]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 03 Jan 2017 ]
    *token = security
    *description = [ CVE-2016-10087 ]
]]

app-arch/libzip[<1.3.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Sep 2017 ]
    token = security
    description = [ CVE-2017-12858, CVE-2017-14107 ]
]]

dev-libs/expat[<2.2.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 18 Jun 2017 ]
    token = security
    description = [ CVE-2016-9063, CVE-2017-9233 ]
]]

media-libs/raptor[<=2.0.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Mar 2012 ]
    token = security
    description = [ CVE-2012-0037 ]
]]

(
    dev-lang/python:2.7[<2.7.11-r2]
    dev-lang/python:3.4[<3.4.3-r6]
    dev-lang/python:3.5[<3.5.1-r2]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 12 Jun 2016 ]
    *token = security
    *description = [ http://www.openwall.com/lists/oss-security/2016/06/11/2 ]
]]

sys-apps/sydbox[~0-scm] [[
    author = [ Ali Polatel <alip@exherbo.org> ]
    date = [ 14 Jun 2012 ]
    token = scm
    description = [ Mask scm version ]
]]

net-libs/libotr[<=3.2.0] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 30 Aug 2012 ]
    token = security
    description = [ CVE-2012-2369 ]
]]

net-print/cups[<2.2.10] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Dez 2018 ]
    token = security
    description = [ CVE-2018-4700 ]
]]

dev-libs/dbus-glib[<0.100.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Mar 2013 ]
    token = security
    description = [ CVE-2013-0292 ]
]]

dev-libs/libxml2[<2.9.4-r3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jan 2017 ]
    token = security
    description = [ CVE-2017-{5969,0663,7375,7376,9047,9048,9049,9050} ]
]]

media-libs/tiff[<4.0.9-r3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Feb 2018 ]
    token = security
    description = [ CVE-2018-5784 ]
]]

sys-apps/dbus[<1.9.10] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Feb 2015 ]
    token = security
    description = [ CVE-2015-0245 ]
]]

app-crypt/gnupg[<2.2.8] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 08 Jun 2018 ]
    token = security
    description = [ CVE-2018-12020 ]
]]

(
    dev-libs/icu:57.1[<57.1-r1]
    dev-libs/icu:58.1[<58.2-r1]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 23 May 2017 ]
    *token = security
    *description = [ CVE-2017-7867, CVE-2017-7868 ]
]]

net-misc/openssh[<7.8_p1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 24 Aug 2018 ]
    token = security
    description = [ CVE-2018-15473 ]
]]

dev-utils/ack[<2.12] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 13 Dez 2013 ]
    token = security
    description = [ http://beyondgrep.com/security/ ]
]]

dev-libs/pinktrace[~0-scm] [[
    author = [ Ali Polatel <alip@exherbo.org> ]
    date = [ 09 Jan 2014 ]
    token = scm
    description = [ Mask scm version ]
]]

sys-apps/file[<5.33] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 16 Apr 2018 ]
    token = security
    description = [ CVE-2018-10360 ]
]]

dev-libs/gnutls[<3.4.15] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 14 Sep 2016 ]
    token = security
    description = [ GNUTLS-SA-2016-3 ]
]]

net-print/cups-filters[<1.4.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Dec 2015 ]
    token = security
    description = [ CVE-2015-8560 ]
]]

app-arch/lzo[<2.07] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Jul 2014 ]
    token = security
    description = [ CVE-2014-4607 ]
]]

net-libs/cyrus-sasl[<2.1.26-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Jul 2014 ]
    token = security
    description = [ CVE-2013-4122 ]
]]

app-crypt/gpgme[<1.5.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 01 Aug 2014 ]
    token = security
    description = [ CVE-2014-3564 ]
]]

dev-libs/libgcrypt[<1.8.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 13 Jun 2018 ]
    token = security
    description = [ CVE-2018-0495 ]
]]

app-shells/bash[<4.3_p30] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Oct 2014 ]
    token = security
    description = [ CVE-2014-6271, CVE-2014-6277, CVE-2014-6278, CVE-2014-7169, CVE-2014-7186, CVE-2014-7187 ]
]]

(
    sys-libs/db:6.1
    sys-libs/db:6.2
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 27 Oct 2014 ]
    *token = testing
    *description = [ Licence changed to AGPL-3, interfering with various packages (e.g. openldap) ]
]]

net-misc/wget[<1.19.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 07 May 2018 ]
    token = security
    description = [ CVE-2018-0494 ]
]]

dev-libs/libksba[<1.3.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 Nov 2014 ]
    token = security
    description = [ CVE-2014-9087 ]
]]

net-dns/bind[<9.12.2_p1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 13 Aug 2018 ]
    token = security
    description = [ CVE-2018-5740 ]
]]

net/ntp[<4.2.8_p12] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Sep 2018 ]
    token = security
    description = [ CVE-2018-12327 ]
]]

dev-scm/subversion[<1.8.14] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Sep 2015 ]
    token = security
    description = [ CVE-2015-318{4,7} ]
]]

(
    dev-libs/libevent:0[<2.0.22-r3]
    dev-libs/libevent:2.1[<2.1.6]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 23 May 2017 ]
    *token = security
    *description = [ CVE-2016-1019{5,6,7} ]
]]

sys-devel/patch[<2.7.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Feb 2015 ]
    token = security
    description = [ CVE-2015-1196 ]
]]

sys-apps/grep[<2.22] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Nov 2015 ]
    token = security
    description = [ CVE-2015-1345 ]
]]

sys-libs/glibc[<2.28-r4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Jan 2019 ]
    token = security
    description = [ CVE-2018-19591 ]
]]

sys-fs/e2fsprogs[<1.42.13] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 May 2015 ]
    token = security
    description = [ CVE-2015-1572 ]
]]

dev-lang/perl:5.26[<5.26.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 02 Dec 2018 ]
    token = security
    description = [ CVE-2018-{12015,18311,18312,18313,18314 ]
]]

dev-lang/perl:5.28[<5.28.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 02 Dec 2018 ]
    token = security
    description = [ CVE-2018-18311, CVE-2018-18312 ]
]]

media-libs/gd[<2.2.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Sep 2017 ]
    token = security
    description = [ CVE-2017-6362, CVE-2017-7890 ]
]]

app-arch/libtar[<1.2.20] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Mar 2015 ]
    token = security
    description = [ CVE-2013-4397 ]
]]

net-libs/libssh2[<1.7.0] [[
    author = [ Kevin Decherf <kevin@kdecherf.com> ]
    date = [ 23 Feb 2016 ]
    token = security
    description = [ CVE-2016-0787 ]
]]

dev-libs/libtasn1[<4.12-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Sep 2017 ]
    token = security
    description = [ CVE-2017-10790 ]
]]

sys-apps/paludis[~2.4.0] [[
    author = [ Thomas Anderson <tanderson@caltech.edu> ]
    date = [ 15 Apr 2015 ]
    description = [ Doesn't support multiarch, downgrading from scm *will* break your system ]
    token = broken
]]

dev-libs/pcre[<8.40-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Jun 2017 ]
    token = security
    description = [ CVE-2017-6004 ]
]]

dev-lang/lua:5.3 [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 12 May 2015 ]
    token = testing
    description = [ Many packages are incompatible with Lua 5.3 ]
]]

dev-db/sqlite:3[<3.23.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 25 Jul 2018 ]
    token = security
    description = [ CVE-2018-8740 ]
]]

app-antivirus/clamav[<0.100.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Oct 2018 ]
    token = security
    description = [ CVE-2018-1{468{0,1,2},5378} ]
]]

dev-libs/xerces-c[<3.1.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 May 2015 ]
    token = security
    description = [ CVE-2015-0252 ]
]]

(
    sys-fs/fuse:0[<2.9.8]
    sys-fs/fuse:3[<3.2.5]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 25 Jul 2018 ]
    *token = security
    *description = [ CVE-2018-10906 ]
]]

sys-libs/pam[<1.2.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 27 Jun 2015 ]
    token = security
    description = [ CVE-2015-3238 ]
]]

sys-apps/less[<475] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Jul 2015 ]
    token = security
    description = [ CVE-2014-9488 ]
]]

net-dns/libidn[<1.33-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Sep 2017 ]
    token = security
    description = [ CVE-2017-14062 ]
]]

sys-fs/xfsprogs[<3.2.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Aug 2015 ]
    token = security
    description = [ CVE-2012-2150 ]
]]

net-nds/rpcbind[<0.2.4-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2017-8779 ]
]]

sys-apps/xinetd[<2.3.15-r3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Nov 2015 ]
    token = security
    description = [ CVE-2013-4342 ]
]]

app-arch/unzip[<6.0-r4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Mar 2018 ]
    token = security
    description = [ CVE-2018-1000035 ]
]]

sys-apps/systemd[<233-r6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Jun 2017 ]
    token = security
    description = [ CVE-2017-9445 ]
]]

app-arch/p7zip[<16.02-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Feb 2018 ]
    token = security
    description = [ CVE-2017-17969, CVE-2018-5996 ]
]]

sys-boot/grub[<2.02-beta2-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Dec 2015 ]
    token = security
    description = [ CVE-2015-8370 ]
]]

dev-libs/libxslt[<1.1.29-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2017-5029 ]
]]

app-misc/screen[<4.5.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Feb 2017 ]
    token = security
    description = [ CVE-2017-5618 ]
]]

dev-libs/nettle[<3.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 21 Oct 2016 ]
    token = security
    description = [ CVE-2016-6489 ]
]]

dev-libs/botan[<1.10.17] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 04 Oct 2017 ]
    token = security
    description = [ CVE-2016-14737 ]
]]

(
    dev-scm/git[<2.18.1]
    dev-scm/git[>=2.19&<2.19.1]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 05 Oct 2018 ]
    *token = security
    *description = [ CVE-2018-17456 ]
]]

sys-apps/busybox[<1.28.0] [[
    author = [ Rasmus Thomsen <cogitri@exherbo.org> ]
    date = [ 26 Mar 2018 ]
    token = security
    description = [ CVE-2017-15873 CVE-2017-15874 CVE-2017-16544 ]
]]

media-gfx/ImageMagick[<6.9.9.33] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 08 Jan 2018 ]
    token = security
    description = [ CVE-2017-17879, CVE-2017-17914 ]
]]

dev-libs/jansson[<2.7-r1] [[
    author = [ Kylie McClain <somasis@exherbo.org> ]
    date = [ 05 May 2016 ]
    token = security
    description = [ CVE-2016-4425 ]
]]

net-wireless/wpa_supplicant[<2.6-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Oct 2017 ]
    token = security
    description = [ CVE-2017-130{77,78,79,80,81,82,86,87,88} ]
]]

net-misc/openntpd[<6.0_p1] [[
    author = [ Kylie McClain <somasis@exherbo.org> ]
    date = [ 31 May 2016 ]
    token = security
    description = [ CVE-2016-5117 ]
]]

media-gfx/GraphicsMagick[<1.3.30] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 Jul 2018 ]
    token = security
    description = [ CVE-2016-2317 ]
]]

app-arch/libarchive[<3.3.2-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Aug 2018 ]
    token = security
    description = [ CVE-2017-14166, CVE-2017-14501, CVE-2017-14503 ]
]]

(
    dev-lang/node[<10.14.0]
    dev-lang/node[>=11&<11.3.0]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 28 Dec 2018 ]
    *token = security
    *description = [ CVE-2018-{073{4,5},1212{1,2,3}} ]
]]

app-arch/p7zip[<15.14.1-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Jun 2016 ]
    token = security
    description = [ CVE-2016-2334, CVE-2016-2335 ]
]]

sys-devel/flex[<2.6.1-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 26 Aug 2016 ]
    token = security
    description = [ CVE-2016-6354 ]
]]

app-arch/tar[<1.31] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jan 2018 ]
    token = security
    description = [ CVE-2018-20482 ]
]]

dev-lang/php[<5.6] [[
    author = [ Kevin Decherf <kevin@kdecherf.com> ]
    date = [ 12 Nov 2016 ]
    token = security
    description = [ End of Life ]
]]

dev-lang/guile:1.8[<1.8.8-r3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 22 Dec 2016 ]
    token = security
    description = [ CVE-2016-8605 ]
]]

dev-lang/guile:2[<2.0.13] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 17 Nov 2016 ]
    token = security
    description = [ CVE-2016-8605 CVE-2016-8606 ]
]]

net-dialup/ppp[<2.4.7-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Nov 2016 ]
    token = security
    description = [ CVE-2015-3310 ]
]]

sys-libs/cracklib[<2.9.6-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 08 Dec 2016 ]
    token = security
    description = [ CVE-2015-6318 ]
]]

sys-libs/musl[<1.1.16] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Jan 2017 ]
    token = security
    description = [ CVE-2016-8859 ]
]]

media-libs/jbig2dec[<0.13-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 31 May 2017 ]
    token = security
    description = [ CVE-2017-{7885,7975,7976,9216} ]
]]

app-text/ghostscript[<9.26] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Nov 2018 ]
    token = security
    description = [ CVE-2018-19409 ]
]]

sys-fs/ntfs-3g_ntfsprogs[<2016.2.22-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 13 Feb 2017 ]
    token = security
    description = [ CVE-2017-0358 ]
]]

app-editors/vim[<8.0.0378] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2017-6350 ]
]]

sys-apps/shadow[<4.5-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Feb 2018 ]
    token = security
    description = [ CVE-2018-7169 ]
]]

net-dns/c-ares[<1.13.0] [[
    author = [ Arnaud Lefebvre <a.lefebvre@outlook.fr> ]
    date = [ 12 Jul 2017 ]
    token = security
    description = [ CVE-2017-1000381 ]
]]

(
    dev-libs/libressl:42.44.16[<2.6.5]
    dev-libs/libressl:43.45.17[<2.7.4]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 13 Jun 2018 ]
    *token = security
    *description = [ CVE-2018-0495, CVE-2018-0732 ]
]]

net-libs/libtirpc[<1.0.2-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Mar 2018 ]
    token = security
    description = [ CVE-2016-4429 ]
]]

(
    dev-lang/go[<1.10.6]
    dev-lang/go[>=1.11&<1.11.3]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 15 Dec 2018 ]
    *token = security
    *description = [ CVE-2018-{16873,16874,16875} ]
]]

dev-libs/libbsd[<0.8.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Feb 2016 ]
    token = security
    description = [ CVE-2016-2090 ]
]]

net-directory/openldap[<2.4.44-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 May 2017 ]
    token = security
    description = [ CVE-2017-9287 ]
]]

sys-devel/binutils[<2.28-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Jun 2017 ]
    token = security
    description = [ CVE-2017-6969, CVE-2017-6966, CVE-2017-6965, CVE-2017-9041, CVE-2017-9040,
                    CVE-2017-9042, CVE-2017-9039, CVE-2017-9038, CVE-2017-8421, CVE-2017-8396,
                    CVE-2017-8397, CVE-2017-8395, CVE-2017-8394, CVE-2017-8393, CVE-2017-8398,
                    CVE-2017-7614 ]
]]

app-arch/unrar[<5.5.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 23 Jun 2017 ]
    token = security
    description = [ CVE-2012-6706 ]
]]

dev-scm/mercurial[<4.3.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Aug 2017 ]
    token = security
    description = [ CVE-2017-1000115, CVE-2017-1000116 ]
]]

dev-scm/subversion[<1.9.7] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Aug 2017 ]
    token = security
    description = [ CVE-2017-9800 ]
]]

sys-apps/coreutils[<8.28] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 2 Sep 2017 ]
    token = security
    description = [ CVE-2017-7476 ]
]]

app-text/podofo[<0.9.5_p20170903] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 21 Sep 2017 ]
    token = security
    description = [ CVE-2017-{5852,5853,5854,5855,5886,6840,6844,6847,7378,
                              7379,7380,7994,8787} ]
]]

net-dns/dnsmasq[<2.78] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Oct 2017 ]
    token = security
    description = [ CVE-2017-13704, CVE-2017-1449{1,2,3,4,5,6} ]
]]

mail-mta/exim[<4.90.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Feb 2018 ]
    token = security
    description = [ CVE-2018-6789 ]
]]

net-misc/rsync[<3.1.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Jan 2018 ]
    token = security
    description = [ CVE-2018-5764 ]
]]

sys-libs/ncurses[<6.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Feb 2018 ]
    token = security
    description = [ CVE-2017-{10684,10685,11112,11113,13728,13729,13730,
                              13731,13732,13733,13734,16879} ]
]]

app-arch/gcab[<1.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 01 Feb 2018 ]
    token = security
    description = [ CVE-2018-5345 ]
]]

net-irc/irssi[<1.1.1] [[
    author = [ Rasmus Thomsen <cogitri@exherbo.org> ]
    date = [ 26 Feb 2018 ]
    token = security
    description = [ CVE-2018-705{0..4} ]
]]

net-dns/idnkit[>=2.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Mar 2018 ]
    token = broken
    description = [ Breaks its solely users bind{,-tools} ]
]]

(
    dev-db/postgresql[<9.6.8]
    dev-db/postgresql[>=10&<10.3]
) [[
    *author = [ Arnaud Lefebvre <a.lefebvre@outlook.fr> ]
    *date = [ 08 Mar 2018 ]
    *token = security
    *description = [ CVE-2018-1058 ]
]]

net-misc/dhcp[<4.3.6_p1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Mar 2018 ]
    token = security
    description = [ CVE-2018-573{2,3} ]
]]

sys-apps/util-linux[<2.32] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 24 Mar 2018 ]
    token = security
    description = [ CVE-2018-7738 ]
]]

(
    dev-lang/php:5.6[<5.6.36]
    dev-lang/php:7.0[<7.0.30]
    dev-lang/php:7.1[<7.1.17]
    dev-lang/php:7.2[<7.2.5]
) [[
    *author = [ Rasmus Thomsen <cogitri@exherbo.org> ]
    *date = [ 10 May 2018 ]
    *token = security
    *description = [ CVE-2018-5712 ]
]]

sys-apps/busybox[<1.28.4-r1] [[
    *author = [ Kylie McClain <somasis@exherbo.org> ]
    *date = [ 7 Jun 2018 ]
    *token = security
    *description = [ Using busybox wget is insecure for https urls, as there is
                     no certificate validation done. Busybox >=1.28.4-r1 changes
                     configuration options to disable HTTPS functionality and
                     error when used. ]
]]

sys-process/procps[<3.3.15] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 03 Jun 2018 ]
    token = security
    description = [ CVE-2018-{1122,1123,1124,1125,1126} ]
]]

dev-libs/crossguid[>=0.2.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jul 2018 ]
    token = testing
    description = [ Breaks its solely user Kodi ]
]]

app-arch/sharutils[<4.15.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Aug 2018 ]
    token = security
    description = [ CVE-2018-1000097 ]
]]

app-arch/cabextract[<1.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Nov 2018 ]
    token = security
    description = [ CVE-2018-18584 ]
]]

dev-libs/boost[~>1.68.0] [[
    author = [ Marvin Schmidt <marv@exherbo.org> ]
    date = [ 30 Oct 2018 ]
    token = testing
    description = [ New versions are known to break dependents ]
]]

dev-util/elfutils[<0.175] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 16 Nov 2018 ]
    token = security
    description = [  CVE-2018-18310, CVE-2018-18520, CVE-2018-18521 ]
]]

sys-devel/gettext[<0.19.8.1-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Jan 2019 ]
    token = security
    description = [ CVE-2018-18751 ]
]]

virtual/libssl [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Jan 2019 ]
    token = pending-removal
    description = [ Superseded by providers ]
]]

virtual/kerberos [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Jan 2019 ]
    token = pending-removal
    description = [ Superseded by providers ]
]]

sys-devel/autoconf-archive[=2019.01.06] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 11 Jan 2019 ]
    token = broken
    description = [ Breaks building sys-apps/dbus ]
]]
