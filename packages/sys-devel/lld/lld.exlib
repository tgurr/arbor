# Copyright 2017 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require cmake [ api=2 ]
require alternatives

SUMMARY="The LLVM linker"
HOMEPAGE="https://lld.llvm.org"
DOWNLOADS="https://releases.llvm.org/${PV}/${PNV}.src.tar.xz"

LICENCES="UoI-NCSA"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-lang/llvm[~${PV}]
    test:
        dev-lang/python:2.7
"

CMAKE_SOURCE=${WORKBASE}/${PNV}.src

src_install() {
    local host=$(exhost --target)

    default

    # provide host-prefixed ld.lld
    dosym lld /usr/${host}/bin/${host}-ld.lld

    # ban ld.lld, alternative for banned ld
    dobanned ld.lld

    # eclectic managed files
    local em=(
        /usr/${host}/bin/${host}-ld
        /usr/${host}/bin/ld
        "${BANNEDDIR}"/ld
    )

    # alternatives setup
    local alternatives=()
    for m in "${em[@]}"; do
        alternatives+=( "${m}" "${m##*/}".lld )
    done

    alternatives_for ld lld 10 "${alternatives[@]}"
}

