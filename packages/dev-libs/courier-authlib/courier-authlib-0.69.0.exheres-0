# Copyright 2010 Alex Elsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ project=courier ]

SUMMARY="A generic authentication API that encapsulates the process of validating account passwords"
DESCRIPTION="
In addition to reading the traditional account passwords from /etc/passwd, the account information
can alternatively be obtained from an LDAP directory; a MySQL or a PostgreSQL database; or a GDBM
or DB file.
"
HOMEPAGE+=" https://www.courier-mta.org/authlib/"

BUGS_TO="Alex Elsayed <eternaleye@gmail.com>"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ldap
    mysql [[ description = [ Support using MySQL for mail user authentication ] ]]
    postgres [[ description = [ Support using PostgreSQL for mail user authentication ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
    build+run:
        dev-libs/gmp:=
        net-libs/courier-unicode[>=2.0]
        net-libs/cyrus-sasl
        sys-libs/gdbm
        ldap? ( net-directory/openldap )
        mysql? ( dev-db/mysql )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        postgres? ( dev-db/postgresql-client )
    run:
        user/courier
        group/courier
    suggestion:
        dev-tcl/expect [[ description = [ Support for changing passwords in Courier's web UI ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    --with-authpam
    --with-authpipe
    --with-authpwd
    --with-authshadow
    --with-authuserdb
    --with-db=gdbm
    --with-mailgroup=courier
    --with-mailuser=courier
    --without-authsqlite
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "ldap authldap"
    "mysql authmysql"
    "postgres authpgsql"
)

src_install() {
    default

    keepdir /var/lib/spool/authdaemon
}

