# Copyright 2009-2012 Pierre Lejeune <superheron@gmail.com>
# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Botan, a C++ crypto library"
DESCRIPTION="
Botan is a BSD-licensed crypto library written in C++.
In provides applications with the ability to use a number
of cryptographic algorithms, as well as X.509 certificates
and CRLs, PKCS #10 certificate requests, a filter/pipe message
processing system, and a wide variety of other features,
all written in portable C++.
"
HOMEPAGE="https://botan.randombit.net/"
MY_PNV="Botan-${PV}"
DOWNLOADS="${HOMEPAGE}releases/${MY_PNV}.tgz"

LICENCES="BSD-2"
SLOT="$(ever major)"
MYOPTIONS="
    boost [[ description = [ Builds Boost.Python wrapper ] ]]
    doc
"

if ever at_least 2.8.0 ; then
    MYOPTIONS+="
        sqlite
        ( providers: bearssl
            ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
        ) [[ number-selected = at-least-one ]]

        ( amd64_cpu_features: ssse3 sse4.1 sse4.2 avx2 )
        ( x86_cpu_features: sse2 ssse3 sse4.1 sse4.2 avx2 )
        ( platform: amd64 x86 )
    "
else
    MYOPTIONS+="
        gmp
        ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
    "
fi

DEPENDENCIES="
    build:
        doc? ( dev-python/Sphinx )
    build+run:
        boost? ( dev-libs/boost )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

if ever at_least 2.7.0 ; then
    DEPENDENCIES+="
        build+run:
            app-arch/bzip2
            app-arch/xz
            sys-libs/zlib
            providers:bearssl? ( dev-libs/bearssl )
            sqlite? ( dev-db/sqlite:3 )
    "
else
    DEPENDENCIES+="
        build+run:
            gmp? ( dev-libs/gmp:=[>=4.1] )
    "
fi

UPSTREAM_CHANGELOG="${HOMEPAGE}/log.html"
UPSTREAM_DOCUMENTATION="http://files.randombit.net/${PN}/api.pdf"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/news/"

WORK="${WORKBASE}/${MY_PNV}"

DEFAULT_SRC_COMPILE_PARAMS=(
    CXX=${CXX}
    LIB_OPT="${CXXFLAGS} -finline-functions"
    RANLIB=${RANLIB}
)

if ever at_least 2 ; then
    DEFAULT_SRC_COMPILE_PARAMS+=( AR="${AR}" )
else
    DEFAULT_SRC_COMPILE_PARAMS+=( AR="${AR} crs" )
    DEFAULT_SRC_INSTALL_PARAMS=(
        DESTDIR="${IMAGE}"/usr/$(exhost --target)
        # configure's --docdic doesn't work below the prefix
        DOCDIR="${IMAGE}"/usr/share/doc/${PNV}
    )
fi

DEFAULT_SRC_TEST_PARAMS=( CXX=${CXX} )
DEFAULT_SRC_INSTALL_EXCLUDE=( readme.txt )

