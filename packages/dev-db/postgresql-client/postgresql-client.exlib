# Copyright 2008, 2009, 2010, 2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases pkg_pretend src_prepare src_compile src_test src_install

if ever at_least 10; then
    PSQL_MAJOR_VERSION=$(ever major)
else
    PSQL_MAJOR_VERSION=$(ever range 1-2)
fi

SUMMARY="front-end programs for PostgreSQL"
DESCRIPTION="
This package contains client and administrative programs for PostgreSQL: these are the interactive terminal client psql and
programs for creating and removing users and databases.

This is the client package for PostgreSQL. If you install PostgreSQL on a standalone machine, you need the server package
dev-db/postgresql, too. On a network, you can install this package on many client machines, while the server package may be installed on
only one machine.

PostgreSQL is an object-relational SQL database management system.
"
HOMEPAGE="https://www.postgresql.org"

BUGS_TO="loki@lokis-chaos.de"
REMOTE_IDS="github:postgres/postgres"

UPSTREAM_DOCUMENTATION="
${HOMEPAGE}/docs/manuals/           [[ lang = en description = [ PostgreSQL Manuals ] ]]
${HOMEPAGE}/docs/faq/               [[ lang = en description = [ FAQ ] ]]
${HOMEPAGE}/docs/${PSQL_MAJOR_VERSION}/static/app-psql.html [[ lang = en description = [ PostgreSQL interactive terminal ] ]]
"

UPSTREAM_RELEASE_NOTES="
${HOMEPAGE}/docs/${PSQL_MAJOR_VERSION}/static/release-${PV//./-}.html
"

SLOT="0"

DOWNLOADS="mirror://postgresql/source/v${PV}/postgresql-${PV}.tar.bz2"

LICENCES="PostgreSQL"

MYOPTIONS="
    doc
    readline [[ description = [ Used only by the client psql for history control and autocomplete funcionality ] ]]
    ssl
    pgdump [[ description = [ Install pg_dump and pg_dumpall. Should only be used if no server is installed! ] ]]
    ssl? ( ( providers: libressl openssl ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    !dev-db/postgresql:0 [[
        description = [ Slotify dev-db/postgresql ]
        resolution = uninstall-blocked-after
    ]]
    build:
        sys-devel/flex
        sys-devel/gettext
        doc? (
            app-text/docbook-sgml-dtd:4.2
            app-text/opensp
            dev-libs/libxml2:=[>=2.6.23]
            dev-libs/libxslt
        )
    build+run:
        readline? ( sys-libs/readline:= )
        ssl? (
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl )
        )
"

WORK="${WORKBASE}/postgresql-${PV}"

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    # 'kerberos krb5'
    readline 'ssl openssl'
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --with-system-tzdata=/usr/share/zoneinfo
    --with-uuid=e2fs
    --with-zlib
)

postgresql-client_pkg_pretend() {
    if option pgdump; then
        ewarn "* pg_dump and pg_dumpall generated files may fail during import in"
        ewarn "* PostgreSQL Servers <${PV}"
        ewarn ""
        ewarn "* do not use the binaries from this package if you need unattended imports"
        ewarn "* in older versions. Instead use the binaries from dev-db/postgresql"
        ewarn ""
        ewarn "* you have been warned!"
    fi
}

postgresql-client_src_prepare() {
    default
    # use /run for unix sockets
    edo sed -e "/DEFAULT_PGSOCKET_DIR/s:/tmp:/run/postgresql:" -i src/include/pg_config_manual.h

    # disable mo file versioning
    expatch "${FILES}/0001-disable-mo-files-versioning.patch"
}


postgresql-client_src_compile() {
    # for client only packages, we need only a subset of all directories
    emake -C src/interfaces
    # we only want a subset of the binaries
    for pgbin in pg_config psql scripts; do
        emake -C src/bin/${pgbin}
    done

    if option pgdump; then
        emake -C src/bin/pg_dump
    fi

    # TODO we should select which documentation we realy need!
    if option doc; then
        emake -C doc/src
    fi
}


postgresql-client_src_install() {
    # install the relevant client headers
    dodir /usr/$(exhost --target)/include/libpq
    dodir /usr/$(exhost --target)/include/internal
    dodir /usr/$(exhost --target)/include/internal/libpq

    insinto /usr/$(exhost --target)/include/
    set -x
    for header in postgres_ext.h pg_config{,_ext,_os}.h pg_config_manual.h ; do
        doins $(readlink -fn src/include/${header})
    done
    set +x

    insinto /usr/$(exhost --target)/include/libpq
    doins src/include/libpq/libpq-fs.h

    insinto /usr/$(exhost --target)/include/internal
    for header in c.h port.h postgres_fe.h ; do
        doins $(readlink -fn src/include/${header})
    done

    insinto /usr/$(exhost --target)/include/internal/libpq
    doins src/include/libpq/pqcomm.h

    # libraries
    emake DESTDIR="${IMAGE}" -C src/interfaces install

    # bin
    # we only want a subset of the binaries
    for pgbin in pg_config psql scripts; do
        emake DESTDIR="${IMAGE}" -C src/bin/${pgbin} install
    done

    if option pgdump; then
        emake DESTDIR="${IMAGE}" -C src/bin/pg_dump install
    fi
    # TODO: clean up here, we only want the client manpages
    if option doc; then
        emake DESTDIR="${IMAGE}" -C doc/src install
    fi

    # clean up the image
    for binary in reindexdb vacuumdb; do
        nonfatal edo rm "${IMAGE}"/usr/$(exhost --target)/bin/${binary}
        nonfatal edo rm "${IMAGE}"/usr/share/locale/*/LC_MESSAGES/${binary}.mo
    done
}

postgresql-client_src_test() {
    :;
    # TODO no tests defined yet
}

