# Copyright 2012 Quentin "Sardem FF7" Glidic <sardemff7+exherbo@sardemff7.net>
# Distributed under the terms of the GNU General Public License v2

require systemd-service github [ project=otp tag="OTP-${PV}" ]

export_exlib_phases src_prepare src_install

myexparam pv=$(ever replace 1 B $(ever replace 2 - ${PV}))

MY_PNV=otp_src_${PV}

SUMMARY="A language to build massively scalable soft real-time systems with requirements on high availability"
HOMEPAGE="http://www.erlang.org/"

LICENCES="EPL-1.1"
SLOT="0"

MYOPTIONS="odbc systemd wxwidgets
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
    build+run:
        sys-libs/ncurses
        sys-libs/zlib
        odbc? ( dev-db/unixODBC )
        systemd? ( sys-apps/systemd )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        group/epmd
        user/epmd
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=docdir
    --hates=datarootdir
    --enable-dynamic-ssl-lib
    --enable-kernel-poll
    --enable-shared-zlib
    --enable-smp-support
    --enable-threads
    --with-ssl
    --with-termcap
    --without-javac
)

if ever at_least 20.3.8; then
    DEPENDENCIES+="
        build+run:
            wxwidgets? ( x11-libs/wxGTK:= )
    "
else
    DEPENDENCIES+="
        build+run:
            wxwidgets? ( x11-libs/wxGTK:2.8 )
    "
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --with-wx-config=/usr/$(exhost --target)/lib/wx/config/gtk2-unicode-release-2.8
    )
fi

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( systemd )

erlang_src_prepare() {
    default

    option odbc || echo "odbc" >> lib/SKIP-APPLICATIONS
    option wxwidgets || echo "wx" >> lib/SKIP-APPLICATIONS

    edo ./otp_build autoconf
}

erlang_src_install() {
    default

    edo find "${IMAGE}" -type d -empty -exec rmdir '{}' +

    install_systemd_files
}

