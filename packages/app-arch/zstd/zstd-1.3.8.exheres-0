# Copyright 2017 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=facebook release=v${PV} suffix=tar.gz ] \
    cmake [ api=2 ]

SUMMARY="Zstandard - Fast real-time compression algorithm"
DESCRIPTION="
Zstandard is a real-time compression algorithm, providing high compression ratios. It offers a very
wide range of compression / speed trade-off, while being backed by a very fast decoder. It also
offers a special mode for small data, called dictionary compression, and can create dictionaries
from any sample set.
"
HOMEPAGE+=" https://facebook.github.io/zstd/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES=""

CMAKE_SOURCE=${WORKBASE}/${PNV}/build/cmake

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
    -DMAN_INSTALL_DIR:PATH=/usr/share/man/man1
    -DZSTD_BUILD_CONTRIB:BOOL=TRUE
    -DZSTD_BUILD_PROGRAMS:BOOL=TRUE
    -DZSTD_BUILD_SHARED:BOOL=TRUE
    -DZSTD_BUILD_STATIC:BOOL=TRUE
    -DZSTD_LEGACY_SUPPORT:BOOL=FALSE
    -DZSTD_LZMA_SUPPORT:BOOL=FALSE
    -DZSTD_MULTITHREAD_SUPPORT=BOOL=TRUE
    -DZSTD_ZLIB_SUPPORT:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DZSTD_BUILD_TESTS:BOOL=TRUE -DZSTD_BUILD_TESTS:BOOL=FALSE'
)

src_install() {
    cmake_src_install

    # tests require the static library being built so we can't disable it
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/libzstd.a
}

